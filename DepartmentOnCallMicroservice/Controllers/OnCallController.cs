﻿using System;
using System.Collections;
using Microsoft.SharePoint.Client;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Http;
using DepartmentOnCallMicroservice.Models;
using NLog.Config;
using NLog;

//using NLog;

namespace DepartmentOnCallMicroservice.Controllers
{
    [RoutePrefix("api/oncall")]
    public class OnCallController : ApiController
    {
        //private static readonly string WebTeam = ConfigurationManager.AppSettings["WebTeam"];
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public OnCallController()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs\\OnCallScheduler{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
        }
        // GET: api/OnCall
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpPost]
        [Route("AddSchedule")]
        public IHttpActionResult AddSchedule()
        {
            var headers = Request.Headers;
            var url = "";
            if (headers.Contains("Url"))
            {
                url = headers.GetValues("Url").First();
            }

            if (string.IsNullOrEmpty(url)) return BadRequest("You did not specify URL");

            var personList = new ArrayList();
            // get people
            try
            {
                using (var context = new ClientContext(url))
                {
                    context.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                    var list = context.Web.Lists.GetByTitle("Persons");
                    var view = list.Views.GetByTitle("All Items");

                    context.Load(view);

                    var query = new CamlQuery();
                    if (context.HasPendingRequest)
                    {
                        context.ExecuteQuery();
                    }

                    query.ViewXml = $"<View><Query>{view.ViewQuery}{query}</Query></View>";

                    var persons = list.GetItems(query);

                    context.Load(persons);

                    if (context.HasPendingRequest)
                    {
                        context.ExecuteQuery();
                    }
                    
                    foreach(var person in persons)
                    {
                        var p = new Persons
                        {
                            Title = person["Title"]?.ToString(),
                            Person = person["Person"]?.ToString(),
                            Phone1 = person["Phone_x0020_1"]?.ToString(),
                            PhoneType1 = person["Phone_x0020_1_x0020_Type"]?.ToString(),
                            Phone2 = person["Phone_x0020_2"]?.ToString(),
                            PhoneType2 = person["Phone_x0020_2_x0020_Type"]?.ToString(),
                            Phone3 = person["Phone_x0020_3"]?.ToString(),
                            PhoneType3 = person["Phone_x0020_3_x0020_Type"]?.ToString(),
                            Phone4 = person["Phone_x0020_4"]?.ToString(),
                            PhoneType4 = person["Phone_x0020_4_x0020_Type"]?.ToString()
                        };
                        personList.Add(p);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception :: AddScheduleWeb :: Getting People :: adding to on call sharepoint " + e.Message);
                return BadRequest(e.Message);
            }
            // get current schedule
            try
            {

                using (var context = new ClientContext(url))
                {
                    context.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                    var list = context.Web.Lists.GetByTitle("Team Schedule");
                    var camlQuery = new CamlQuery
                    {
                        ViewXml =
                            "<View><Query><Where><Eq><FieldRef Name='Start_x0020_Date_x0020_Time' /><Value Type='DateTime'><Today/></Value></Eq></Where></Query></View>"
                    };
                    var collListItem = list.GetItems(camlQuery);

                    context.Load(collListItem);
                    context.ExecuteQuery();

                    if (collListItem.Count != 0) return Ok("Schedule already created for this week");
                    var success = AddToList(personList, url);
                    if (success)
                    {
                        return Ok();
                    }
                    Logger.Error("AddScheduleWeb :: Failed to add new schedule.");
                    return BadRequest("AddScheduleWeb :: Failed to add new schedule.");

                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception :: AddScheduleWeb :: adding to on call sharepoint " + e.Message);
                return BadRequest(e.Message);
            }
        }

        public static bool AddToList(ArrayList personList, string url)
        {
            try
            {
                var numRows = personList.Count - 1; // don't include escalation (Lead) in the calculation
                // get "numRows" from the scheduleList ordered by startDate
                using (var context = new ClientContext(url))
                {
                    context.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                    var list = context.Web.Lists.GetByTitle("Team Schedule");
                    var camlQuery = new CamlQuery
                    {
                        ViewXml =
                            "<View><Query><OrderBy><FieldRef Name='Start_x0020_Date_x0020_Time' Ascending='FALSE'/></OrderBy></Query><RowLimit>"+numRows+"</RowLimit></View>"
                    };
                    var collListItem = list.GetItems(camlQuery);

                    context.Load(collListItem);
                    context.ExecuteQuery();
                    var last = collListItem[collListItem.Count-1];// this will be the new one
                    var primaryName = (FieldLookupValue)last["Primary"];
                    var lookupValue = primaryName.LookupValue;

                    var found = false;
                    foreach (Persons person in personList)
                    {
                        if (!person.Title.Equals(lookupValue))
                            continue;
                        found = true;
                        break;
                    }
                    if (!found)
                    {
                        last = collListItem[collListItem.Count - 2]; // get next one
                    }
                    var itemCreateInfo = new ListItemCreationInformation();
                    var newItem = list.AddItem(itemCreateInfo);
                    newItem["Title"] = "Week of " + DateTime.Now.Date;
                    var startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 16, 00, 00);
                    newItem["Start_x0020_Date_x0020_Time"] = startDate;
                    var sevenDays = startDate.AddDays(7);
                    var endDate = new DateTime(sevenDays.Year, sevenDays.Month, sevenDays.Day, 15, 59, 00);
                    newItem["End_x0020_Date_x0020_Time"] = endDate;
                    newItem["Primary"] = last["Primary"];
                    newItem["Escalation_x0020_1_x002f_Seconda"] = last["Escalation_x0020_1_x002f_Seconda"];
                    newItem["Escalation_x0020_2"] = last["Escalation_x0020_2"];
                    newItem["Wt"] = 0;
                    newItem.Update();
                    context.ExecuteQuery();
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception :: AddToList :: adding to on call sharepoint " + e.Message);
                return false;
            }
            
            return true;
        }
    }
}
