﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DepartmentOnCallMicroservice.Models
{
    public class Persons
    {
        public string Title { get; set; }
        public string Person { get;set; }
        public string PhoneType1 { get; set; }
        public string Phone1 { get; set; }
        public string PhoneType2 { get; set; }
        public string Phone2 { get; set; }
        public string PhoneType3 { get; set; }
        public string Phone3 { get; set; }
        public string PhoneType4 { get; set; }
        public string Phone4 { get; set; }
        public string Other { get; set; }
    }
}